﻿namespace DesafioAutores.App
{
    public interface IIOHandler
    {
        string ReadText();
        int ReadInt();
        void ShowText(string text);
    }
}
