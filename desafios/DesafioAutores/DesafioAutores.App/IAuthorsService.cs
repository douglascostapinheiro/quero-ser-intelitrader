﻿namespace DesafioAutores.App
{
    public interface IAuthorsService
    {
        string FormatAuthorName(string rawName);
    }
}
