﻿using System;

namespace DesafioAutores.App
{
    public class IOHandler : IIOHandler
    {
        public string ReadText()
        {
            return Console.ReadLine();
        }

        public int ReadInt()
        {
            string value = this.ReadText();
            return int.Parse(value);
        }

        public void ShowText(string text)
        {
            Console.WriteLine(text);
        }
    }
}
