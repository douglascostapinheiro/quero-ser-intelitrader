﻿using System.Collections.Generic;
using System.Linq;

namespace DesafioAutores.App
{
    public class AuthorsService : IAuthorsService
    {
        public string FormatAuthorName(string rawName)
        {
            List<string> splittedName = this.SplitName(rawName);

            if (IsSingleName(splittedName)) return FormatLastName(rawName);

            string lastName = splittedName.Last();
            string restOfTheName = splittedName.First();

            return $"{FormatLastName(lastName)}, {FormatRestOfTheName(restOfTheName)}";
        }

        private List<string> SplitName(string rawName)
        {
            List<string> splittedName = rawName.Split(" ").ToList();

            if (IsSingleName(splittedName)) return new List<string> { rawName };

            List<string> result = new List<string>();
            if (splittedName.Count > 2 && IsParentName(splittedName.Last()))
            {
                result.Add(string.Join(" ", splittedName.Take(splittedName.Count - 2)));
                result.Add(string.Join(" ", splittedName.Skip(splittedName.Count - 2)));
            }
            else
            {
                result.Add(string.Join(" ", splittedName.Take(splittedName.Count - 1)));
                result.Add(string.Join(" ", splittedName.Skip(splittedName.Count - 1)));
            }

            return result;
        }

        private bool IsSingleName(List<string> splittedName)
        {
            return splittedName.Count == 1;
        }

        private bool IsPrepositions(string name)
        {
            string[] prepositions = new string[] { "da", "de", "do", "das", "dos" };
            return (prepositions.Any(d => d == name.ToLower()));
        }

        private bool IsParentName(string name)
        {
            string[] parentNames = new string[] { "filho", "filha", "neto", "neta", "sobrinho", "sobrinha" };
            return (parentNames.Any(d => d == name.ToLower()));
        }

        private string FormatLastName(string lastName)
        {
            return lastName.ToUpper();
        }

        private string FormatRestOfTheName(string restOfTheName)
        {
            List<string> result = new List<string>();

            foreach (var item in restOfTheName.Split(" ").ToList())
            {
                if (IsPrepositions(item)) result.Add(item.ToLower());
                else result.Add(item.First().ToString().ToUpper() + item.Substring(1).ToLower());
            }

            return string.Join(" ", result);
        }


    }
}
