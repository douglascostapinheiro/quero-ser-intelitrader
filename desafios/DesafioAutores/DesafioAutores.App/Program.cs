﻿using System.Collections.Generic;

namespace DesafioAutores.App
{
    class Program
    {
        static void Main(string[] args)
        {
            // URL do Desafio: http://dojopuzzles.com/problemas/exibe/nomes-de-autores-de-obras-bibliograficas/

            IIOHandler ioHandler = new IOHandler();
            IAuthorsService authorService = new AuthorsService();

            ioHandler.ShowText("Olá, bem vindo ao desafio de autores, para começar, digite o numero de autores que vai cadastrar: ");
            int numAutores = 0;

            while (numAutores < 1)
            {
                try
                {
                    numAutores = ioHandler.ReadInt();
                }
                catch
                {
                    ioHandler.ShowText("Por favor, digite um numero inteiro valido, ex: 1, 3, 5, 10, 20: ");
                }
            }

            List<string> authors = new List<string>(numAutores);

            for (int i = 0; i < numAutores; i++)
            {
                ioHandler.ShowText($"Digite o {i + 1}º autor: ");
                authors.Add(ioHandler.ReadText());
            }

            ioHandler.ShowText("\nOs nomes dos autores ficaram assim:");

            foreach (string author in authors)
                ioHandler.ShowText(authorService.FormatAuthorName(author));
        }
    }
}
