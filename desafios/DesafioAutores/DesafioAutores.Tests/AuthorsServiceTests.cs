﻿using DesafioAutores.App;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DesafioAutores.Tests
{
    [TestClass]
    public class AuthorsServiceTests
    {
        [TestMethod]
        public void Test_FormatAuthorsNameToUppercaseWithSingleName()
        {
            string name = "Douglas";
            AuthorsService service = new AuthorsService();

            string result = service.FormatAuthorName(name);

            Assert.AreEqual(name.ToUpper(), result);
        }

        [TestMethod]
        public void Test_FormatAuthorsNameCorrecly()
        {
            string name = "Douglas da Costa Pinheiro";
            AuthorsService service = new AuthorsService();

            string result = service.FormatAuthorName(name);

            Assert.AreEqual("PINHEIRO, Douglas da Costa", result);
        }

        [TestMethod]
        public void Test_FormatAuthorsNameWithCaseWrong()
        {
            string name = "douglas DA costA pinheiro";
            AuthorsService service = new AuthorsService();

            string result = service.FormatAuthorName(name);

            Assert.AreEqual("PINHEIRO, Douglas da Costa", result);
        }

        [TestMethod]
        public void Test_FormatAuthorsNameWithParentNames1()
        {
            string name = "Joao Neto";
            AuthorsService service = new AuthorsService();

            string result = service.FormatAuthorName(name);

            Assert.AreEqual("NETO, Joao", result);
        }

        [TestMethod]
        public void Test_FormatAuthorsNameWithParentNames2()
        {
            string name = "Joao Silva Neto";
            AuthorsService service = new AuthorsService();

            string result = service.FormatAuthorName(name);

            Assert.AreEqual("SILVA NETO, Joao", result);
        }
    }
}
