using DesafioAutores.App;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.IO;

namespace DesafioAutores.Tests
{
    [TestClass]
    public class IOHandlerTests
    {
        [TestMethod]
        public void Test_ReadInputTest_Success()
        {
            string inputText = "test";
            var input = new StringReader(inputText);
            Console.SetIn(input);

            IOHandler handler = new IOHandler();
            string result = handler.ReadText();

            Assert.AreEqual(inputText, result);
        }

        [TestMethod]
        public void Test_ReadInputInteger_FailOnString()
        {
            string inputText = "test";
            var input = new StringReader(inputText);
            Console.SetIn(input);

            IOHandler handler = new IOHandler();
            
            Assert.ThrowsException<FormatException>(() => handler.ReadInt());
        }

        [TestMethod]
        public void Test_ReadInputInteger_Success()
        {
            int inputInt = It.IsAny<int>();
            var input = new StringReader(inputInt.ToString());
            Console.SetIn(input);

            IOHandler handler = new IOHandler();
            int result = handler.ReadInt();

            Assert.AreEqual(inputInt, result);
        }

        [TestMethod]
        public void Test_ShowText_Success()
        {
            string message = "test message:";

            var output = new StringWriter();
            Console.SetOut(output);

            IOHandler handler = new IOHandler();
            handler.ShowText(message);

            string result = output.ToString().Trim();

            Assert.AreEqual(result, message);
        }
    }
}
