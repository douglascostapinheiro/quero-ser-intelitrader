import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {
  result: string = '';

  constructor() { }
  ngOnInit(): void { }

  addValue(value) {
    if(value === "run") this.run();
    else if(value === "del") this.clear();
    else {
      if (this.result === undefined) this.result = "";
      this.result = this.result.toString().concat(value);
    }
  }

  addValueOnResult = value => this.result = value;
  run = () => this.result = eval(this.result);
  clear = () => this.result = "";
}
