function addValueToExpression(value) {
    if (value === "=") run();
    else if (value === "del") deleteExpression();
    else appendValueToExpression(value);
}

function appendValueToExpression(value) {
    let currentValue = document.getElementById("result").value;
    document.getElementById("result").value = currentValue.concat(value);
}

function deleteExpression() {
    document.getElementById("result").value = "";
}

function run() {
    closeAlert();

    let currentValue = document.getElementById("result").value;
    let result = eval(currentValue);

    if (result === Infinity) {
        showErrorMessage();
        return currentValue;
    }
    else document.getElementById("result").value = result;

    return result;
}

function showErrorMessage() {
    document.getElementById("my-alert").classList.add("active");
}

function closeAlert() {
    document.getElementById("my-alert").classList.remove("active");
}

function setInputFilter(textbox, inputFilter) {
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
      textbox.addEventListener(event, function(target) {
          
        if(target.key === "Enter") {
            this.value = run();
        }
        else if (target.key === "Delete") {
            deleteExpression();
            this.value = "";
            this.oldValue = "";
        }
        else if (inputFilter(this.value)) {
          this.oldValue = this.value;
          this.oldSelectionStart = this.selectionStart;
          this.oldSelectionEnd = this.selectionEnd;
        } else if (this.hasOwnProperty("oldValue")) {
          this.value = this.oldValue;
          this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        } else {
          this.value = "";
        }
      });
    });
}

setInputFilter(document.getElementById("result"), function(value) {
    return /[0-9.\+-\/\*]$/.test(value);
});