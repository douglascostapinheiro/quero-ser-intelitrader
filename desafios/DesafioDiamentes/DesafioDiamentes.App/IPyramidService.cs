﻿namespace DesafioDiamentes.App
{
    public interface IPyramidService
    {
        string BuildPyramid(char initialLetter);
    }
}
