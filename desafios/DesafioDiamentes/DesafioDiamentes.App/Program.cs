﻿using System;

namespace DesafioDiamentes.App
{
    class Program
    {
        static void Main(string[] args)
        {
            // url do desafio: http://dojopuzzles.com/problemas/exibe/diamantes/
            IAlphabet alphabet = new Alphabet();
            IPyramidService pyramid = new PyramidService(alphabet);

            Console.WriteLine("Ola, bem vindo ao algoritmo do Diamante, digita uma letra: ");
            char initialLetter = Convert.ToChar(Console.ReadLine().ToUpper());

            Console.WriteLine(pyramid.BuildPyramid(initialLetter));
        }
    }
}
