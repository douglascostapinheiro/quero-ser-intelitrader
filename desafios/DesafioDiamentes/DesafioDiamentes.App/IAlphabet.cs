﻿namespace DesafioDiamentes.App
{
    public interface IAlphabet
    {
        int GetNumberByLetter(char letter);
        char GetLetterByNumber(int number);
    }
}
