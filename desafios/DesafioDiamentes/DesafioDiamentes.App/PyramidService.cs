﻿using System.Collections.Generic;

namespace DesafioDiamentes.App
{
    public class PyramidService : IPyramidService
    {
        private readonly IAlphabet alphabet;

        public PyramidService(IAlphabet alphabet)
        {
            this.alphabet = alphabet;
        }

        public string BuildPyramid(char initialLetter)
        {
            int letterNumber = alphabet.GetNumberByLetter(initialLetter);
            List<string> pyramid = new List<string>();

            pyramid.AddRange(BuildHalfTop(letterNumber));
            pyramid.AddRange(BuildHalfBottom(letterNumber));

            return string.Join("\n", pyramid);
        }

        private List<string> BuildHalfBottom(int letterNumber)
        {
            List<string> pyramid = new List<string>();
            
            for (int i = letterNumber - 1; i >= 0; i--)
            {
                if (i == 0)
                    pyramid.Add(new string(' ', letterNumber) + 'A');
                else
                {
                    char letter = alphabet.GetLetterByNumber(i);
                    pyramid.Add($"{new string(' ', letterNumber - i)}{letter}{new string(' ', (i * 2) - 1)}{letter}");
                }
            }

            return pyramid;
        }

        private List<string> BuildHalfTop(int letterNumber)
        {
            List<string> pyramid = new List<string>();

            for (int i = 0; i <= letterNumber; i++)
            {
                if (i == 0)
                    pyramid.Add(new string(' ', letterNumber) + 'A');
                else
                {
                    char letter = alphabet.GetLetterByNumber(i);
                    pyramid.Add($"{new string(' ', letterNumber - i)}{letter}{new string(' ', (i * 2) - 1)}{letter}");
                }
            }

            return pyramid;
        }
    }
}
