﻿namespace DesafioDiamentes.App
{
    public class Alphabet : IAlphabet
    {
        private readonly string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        public int GetNumberByLetter(char letter) => letters.IndexOf(letter);
        public char GetLetterByNumber(int number) => letters[number];
    }
}
