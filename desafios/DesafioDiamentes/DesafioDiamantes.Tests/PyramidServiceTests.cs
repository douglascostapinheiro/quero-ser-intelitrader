﻿using DesafioDiamentes.App;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DesafioDiamantes.Tests
{
    [TestClass]
    public class PyramidServiceTests
    {
        [TestMethod]
        public void Test_ShouldReturnThePyramidCorrectly()
        {
            PyramidService pyramid = new PyramidService(new Alphabet());

            string expected =
                "  A\n" +
                " B B\n" +
                "C   C\n" +
                " B B\n" +
                "  A";

            string result = pyramid.BuildPyramid('C');

            Assert.AreEqual(expected, result);
        }
    }
}
