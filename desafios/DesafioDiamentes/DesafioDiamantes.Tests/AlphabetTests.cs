﻿using DesafioDiamentes.App;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace DesafioDiamantes.Tests
{
    [TestClass]
    public class AlphabetTests
    {
        [TestMethod]
        public void Test_ShouldReturnTheCorrectlyNumber()
        {
            Alphabet alphabet = new Alphabet();
            Dictionary<char, int> dict = GetDict();

            foreach (var item in dict)
                Assert.AreEqual(dict[item.Key], alphabet.GetNumberByLetter(item.Key));
        }


        [TestMethod]
        public void Test_ShouldReturnTheCorrectlyLetter()
        {
            Alphabet alphabet = new Alphabet();
            Dictionary<char, int> dict = GetDict();

            foreach (var item in dict)
                Assert.AreEqual(dict.First(d => d.Value == item.Value).Key, alphabet.GetLetterByNumber(item.Value));
        }

        private Dictionary<char, int> GetDict()
        {
            Dictionary<char, int> dict = new Dictionary<char, int>();

            dict.Add('A', 0);
            dict.Add('B', 1);
            dict.Add('C', 2);
            dict.Add('D', 3);
            dict.Add('E', 4);
            dict.Add('F', 5);
            dict.Add('G', 6);
            dict.Add('H', 7);
            dict.Add('I', 8);
            dict.Add('J', 9);
            dict.Add('K', 10);
            dict.Add('L', 11);
            dict.Add('M', 12);
            dict.Add('N', 13);
            dict.Add('O', 14);
            dict.Add('P', 15);
            dict.Add('Q', 16);
            dict.Add('R', 17);
            dict.Add('S', 18);
            dict.Add('T', 19);
            dict.Add('U', 20);
            dict.Add('V', 21);
            dict.Add('W', 22);
            dict.Add('X', 23);
            dict.Add('Y', 24);
            dict.Add('Z', 25);

            return dict;
        }
    }
}
