using Microsoft.VisualStudio.TestTools.UnitTesting;
using SequenciaLookAndSay.App;
using System;
using System.Linq;

namespace SequenciaLookAndSay.Tests
{
    [TestClass]
    public class LookAndSayServiceTests
    {
        [TestMethod]
        public void Test_GenerateSimpleSequence()
        {
            ILookAndSayService service = new LookAndSayService();
            int initial = 1;
            int length = 2;

            var result = service.GenerateSequence(initial, length);
            string expected = "1 � descrito como 'um 1' ou 11;\n" +
                "11 � descrito como 'dois 1' ou 21;\n";

            Assert.AreEqual(expected, string.Join("", result.ToArray()));
        }

        [TestMethod]
        public void Test_GenerateSequenceALittleMoreComplex()
        {
            ILookAndSayService service = new LookAndSayService();
            int initial = 1;
            int length = 5;

            var result = service.GenerateSequence(initial, length);
            string expected = "1 � descrito como 'um 1' ou 11;\n" +
                "11 � descrito como 'dois 1' ou 21;\n" +
                "21 � descrito como 'um 2, um 1' ou 1211;\n" +
                "1211 � descrito como 'um 1, um 2, dois 1' ou 111221;\n" +
                "111221 � descrito como 'tr�s 1, dois 2, um 1' ou 312211;\n";

            Assert.AreEqual(expected, string.Join("", result.ToArray()));
        }

        [TestMethod]
        public void Test_GetLastLine()
        {
            ILookAndSayService service = new LookAndSayService();
            int initial = 1;
            int length = 5;

            var result = service.GetLastLine(initial, length);
            string expected = "111221 � descrito como 'tr�s 1, dois 2, um 1' ou 312211;\n";

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void Test_GetLastLineCount()
        {
            ILookAndSayService service = new LookAndSayService();
            int initial = 1;
            int length = 5;

            var result = service.GetLastLineCount(initial, length);
            int expected = 10;

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void Test_GetNextNumber()
        {
            ILookAndSayService service = new LookAndSayService();

            string result = service.GenerateNextNumber("111221");
            string expected = "312211";

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void Test_NumberToNominal()
        {
            ILookAndSayService service = new LookAndSayService();

            string result = service.NumberToNominal("111221");
            string expected = "tr�s 1, dois 2, um 1";

            Assert.AreEqual(expected, result);
        }
    }
}
