﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SequenciaLookAndSay.App
{
    public class LookAndSayService : ILookAndSayService
    {
        public IList<string> GenerateSequence(int initial, int lenght)
        {
            List<string> sequences = new List<string>();
            
            string newNumber = initial.ToString();

            for (int i = 0; i < lenght; i++)
            {
                string newLine = BuildOneLine(newNumber);
                newNumber = newLine.Split(" ou ").Last();
                sequences.Add($"{newLine};\n");
            }

            return sequences;
        }

        public string GetLastLine(int initial, int lenght)
        {
            return this.GenerateSequence(initial, lenght).Last();
        }

        public int GetLastLineCount(int initial, int lenght)
        {
            string lastNumber = GetLastLine(initial, lenght).Split(" ou ").Last().Split(";").First();
            string[] lastNumberSplitted = lastNumber.Select(digit => digit.ToString()).ToArray();
            return lastNumberSplitted.Sum(d => Convert.ToInt32(d));
        }

        private string BuildOneLine(string actualNumber)
        {
            string lineResult = $"{actualNumber} é descrito como '";

            string newNumber = this.GenerateNextNumber(actualNumber);
            string newNumberNominal = this.NumberToNominal(actualNumber);

            lineResult = $"{lineResult}{newNumberNominal}' ou {newNumber}";

            return lineResult;
        }

        public string NumberToNominal(string value)
        {
            var numbersDictionary = this.GetNumberTranslations();
            string[] inputSplitted = value.Select(digit => digit.ToString()).ToArray();
            List<string> rows = new List<string>();

            int charactersCount = 1;
            for (int j = 0; j < inputSplitted.Length; j += charactersCount)
            {
                charactersCount = 1;

                for (int k = j + 1; k < inputSplitted.Length; k++)
                {
                    if (inputSplitted[j] == inputSplitted[k]) charactersCount++;
                    else break;
                }

                rows.Add($"{numbersDictionary[charactersCount]} {inputSplitted[j]}");
            }

            return string.Join(", ", rows);
        }

        public string GenerateNextNumber(string value)
        {
            string[] inputSplitted = value.Select(digit => digit.ToString()).ToArray();
            string result = "";

            int charactersCount = 1;
            for (int j = 0; j < inputSplitted.Length; j += charactersCount)
            {
                charactersCount = 1;

                for (int k = j + 1; k < inputSplitted.Length; k++)
                {
                    if (inputSplitted[j] == inputSplitted[k]) charactersCount++;
                    else break;
                }

                result = $"{result}{charactersCount}{inputSplitted[j]}";
            }

            return result;
        }

        private IDictionary<int, string> GetNumberTranslations()
        {
            IDictionary<int, string> dict = new Dictionary<int, string>();

            dict.Add(0, "zero");
            dict.Add(1, "um");
            dict.Add(2, "dois");
            dict.Add(3, "três");
            dict.Add(4, "quatro");
            dict.Add(5, "cinco");
            dict.Add(6, "seis");
            dict.Add(7, "sete");
            dict.Add(8, "oito");
            dict.Add(9, "nove");

            return dict;
        }
    }
}
