﻿using System;

namespace SequenciaLookAndSay.App
{
    class Program
    {
        static void Main(string[] args)
        {
            // url do desafio: http://dojopuzzles.com/problemas/exibe/sequencia-look-and-say/
            ILookAndSayService service = new LookAndSayService();
            int limit = 5; // 50 is a late 

            Console.WriteLine("Ola, bem vindo ao desafio da sequencia look and say, digite um numero para começar: ");
            int intial = int.Parse(Console.ReadLine());

            // To show all the sequence
            Console.WriteLine(string.Join("", service.GenerateSequence(intial, limit)));

            int result = service.GetLastLineCount(intial, limit);

            Console.WriteLine($"O resultado final do calculo dos elementos da linha {limit} eh {result}");
        }
    }
}
