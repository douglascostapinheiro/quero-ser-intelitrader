﻿using System.Collections.Generic;

namespace SequenciaLookAndSay.App
{
    public interface ILookAndSayService
    {
        IList<string> GenerateSequence(int initial, int lenght);
        string GetLastLine(int initial, int lenght);
        int GetLastLineCount(int initial, int lenght);
        string GenerateNextNumber(string value);
        string NumberToNominal(string value);
    }
}
